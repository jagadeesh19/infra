resource "aws_route53_zone" "primary" {
  name = "jagadeeshreddy.net"
}

resource "aws_route53domains_registered_domain" "jagadeeshreddy" {
  domain_name = "jagadeeshreddy.net"

  dynamic "name_server" {
    for_each = aws_route53_zone.primary.name_servers
    content {
      name = name_server.value
    }
  }
}

resource "aws_acm_certificate" "cert" {
  domain_name = "jagadeeshreddy.net"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }

  subject_alternative_names = [
    "www.jagadeeshreddy.net",
    "mal-client.jagadeeshreddy.net"
  ]
}

resource "aws_acm_certificate_validation" "validation" {
  certificate_arn = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in aws_route53_record.verification : record.fqdn]
}

resource "aws_route53_record" "verification" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }
  name    = each.value.name
  records    = [each.value.record]
  type = each.value.type
  ttl = 60
  zone_id = aws_route53_zone.primary.zone_id
  allow_overwrite = true
}
